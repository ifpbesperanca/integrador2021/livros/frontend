import axios from 'axios';

const getLivros = async () => {
  return await axios.get('https://livrosbackend.hugofigueiredo.repl.co/livro', {
  headers: {
    Authorization:"bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRlc3RlIiwiaWF0IjoxNjM5NzEyMzI1LCJleHAiOjE2Mzk3MTU5MjV9._ezGgpSX_Ji-9bZYiujFK3YZaGaWFLe8Q4-RzMp0Duk"
  }});
}

const addLivro = async (livro) => {
  return await axios.post('https://livrosbackend.hugofigueiredo.repl.co/livro',livro, {headers: {
    Authorization:"bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6InRlc3RlIiwiaWF0IjoxNjM5NzEyMzI1LCJleHAiOjE2Mzk3MTU5MjV9._ezGgpSX_Ji-9bZYiujFK3YZaGaWFLe8Q4-RzMp0Duk"
  }} );
}

const updateLivro  = async (livro) => {
  return await axios.put('https://livrosbackend.hugofigueiredo.repl.co/livro', livro);
}

export {getLivros, addLivro, updateLivro};