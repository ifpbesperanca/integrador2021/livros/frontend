import React, { useState, useRef, useEffect } from 'react';

import { classNames } from 'primereact/utils';
import { InputText } from 'primereact/inputtext';
import { InputNumber } from 'primereact/inputnumber';
import { Button } from 'primereact/button';
import { Toast } from 'primereact/toast';
import { Toolbar } from 'primereact/toolbar';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { Dialog } from 'primereact/dialog';
import PrimeReact from 'primereact/api';

import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'primereact/resources/themes/saga-green/theme.css'
import {getLivros, addLivro, updateLivro} from './LivroService'

//efeito nos botões ao clicar
PrimeReact.ripple = true;

export default () => {
    let emptyLivro = {
        id: null,
        titulo: '',
        editora: '',
        autores: ''
    };

    const [livros, setLivros] = useState(null);
    const [livroDialog, setLivroDialog] = useState(false);
    const [livro, setLivro] = useState(emptyLivro);
    const [selectedLivros, setSelectedLivros] = useState(null);
    const [submitted, setSubmitted] = useState(false);
    const [globalFilter, setGlobalFilter] = useState(null);
    const toast = useRef(null);
    
    const atualiza = () => useEffect(() => {
        getLivros().then(res => {setLivros(res.data); console.log(res.data)});
    }, []); 

    atualiza();
 

    const openNew = () => {
        setLivro(emptyLivro);
        setSubmitted(false);
        setLivroDialog(true);
    }

    const hideDialog = () => {
        setSubmitted(false);
        setLivroDialog(false);
    }


    const saveLivro = () => {
      setSubmitted(true);
      if (livro.id) {     
        updateLivro(livro);    
        toast.current.show({ severity: 'success', summary: 'Sucesso', detail: 'Livro Atualizado', life: 3000 });
      } else {
         addLivro(livro);
        toast.current.show({ severity: 'success', summary: 'Sucesso', detail: 'Livro Adicionado', life: 3000 });
      }
      setLivroDialog(false);
      setLivro(emptyLivro);
      atualiza();
    }

    const editLivro = (livro) => {
        setLivro({...livro});
        setLivroDialog(true);
    }

    const onInputChange = (e, name) => {
        const val = e?.target?.value || '';
        let _livro = {...livro};
        _livro[`${name}`] = val;

        setLivro(_livro);
    }

    const onInputNumberChange = (e, name) => {
        const val = e.value || 0;
        let _livro = {...livro};
        _livro[`${name}`] = val;

        setLivro(_livro);
    }

    const leftToolbarTemplate = () => {
        return (
            <>
                <Button label="New" icon="pi pi-plus" className="p-button-success p-mr-2" onClick={openNew} />
            </>
        )
    }
 
    const actionBodyTemplate = (rowData) => {
        return (
            <>
                <Button icon="pi pi-pencil" className="p-button-rounded p-button-success p-mr-2" onClick={() => editLivro(rowData)} />
            </>
        );
    }

    const capaBodyTemplate = (rowData) => {
      if(!rowData.capa) return;
      const base64String = btoa(String.fromCharCode(...new Uint8Array(rowData.capa.data)));
        return (
            <>
              <img src={`data:image/jpg;base64,${base64String}`} width="100%" height="auto" />
            </>
        );
    }
    

    const header = (
        <div className="table-header">
            <h5 className="p-m-0">Gerenciador de Livros</h5>
            <span className="p-input-icon-left">
                <i className="pi pi-search" />
                <InputText type="search" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Search..." />
            </span>
        </div>
    );
    const livroDialogFooter = (
        <>
            <Button label="Cancel" icon="pi pi-times" className="p-button-text" onClick={hideDialog} />
            <Button label="Save" icon="pi pi-check" className="p-button-text" onClick={saveLivro} />
        </>
    );
    
    return (
        <div className="datatable-crud-demo">
            <Toast ref={toast} />

            <div className="card">
                <Toolbar className="p-mb-4" left={leftToolbarTemplate}></Toolbar>

                <DataTable value={livros} dataKey="id" paginator rows={10} rowsPerPageOptions={[5, 10, 25]}
                    paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
                    currentPageReportTemplate="Apresentando {first} até {last} de {totalRecords} livros"
                    globalFilter={globalFilter}
                    header={header}>

                    <Column field="id" header="Id" sortable></Column>
                    <Column field="titulo" header="Título" sortable></Column>
                    <Column field="editora" header="Editora" sortable></Column>
                    <Column field="autores" header="Autores" sortable></Column>
                    <Column body={capaBodyTemplate}></Column>
                    <Column body={actionBodyTemplate}></Column>
                </DataTable>
            </div>

            <Dialog visible={livroDialog} style={{ width: '450px' }} header="Detalhes Livro" modal className="p-fluid" footer={livroDialogFooter} onHide={hideDialog}>
                <div className="p-field">
                    <label htmlFor="titulo">Título</label>
                    <InputText id="titulo" value={livro.titulo} onChange={(e) => onInputChange(e, 'titulo')} required autoFocus className={classNames({ 'p-invalid': submitted && !livro.titulo })} />
                    {submitted && !livro.titulo && <small className="p-error">Name is required.</small>}
                </div>
                <div className="p-formgrid p-grid">
                    <div className="p-field p-col">
                        <label htmlFor="editora">Editora</label>
                        <InputText id="editora" value={livro.editora} onChange={(e) => onInputChange(e, 'editora')} required autoFocus className={classNames({ 'p-invalid': submitted && !livro.editora })} />
                    </div>
                    <div className="p-field p-col">
                        <label htmlFor="autores">Autores</label>
                        <InputText id="autores" value={livro.autores} onChange={(e) => onInputChange(e, 'autores')} required autoFocus className={classNames({ 'p-invalid': submitted && !livro.autores })} />
                    </div>
                </div>
            </Dialog>

          </div>
    );
}